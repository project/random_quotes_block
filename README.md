# Random Quote block

Random quotes block provide a content type where you can create content. 
It's provide you a "random quotes block" block and you can place this block 
according to your requirement and this block show randomly content from 
"rand block content type".

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/random_quotes_block).

- Submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/random_quotes_block).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.


## Troubleshooting

If not displayed updated content, check the following:

- Check you have administrator permission to view this module.
- Clear the drupal cache if is not displayed updated content.


## Maintainers

- Jitendra Kumar - [Jitendra_Kumar](https://www.drupal.org/u/jitendra_kumar)
- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Kamlesh Kishor Jha - [Kamlesh Kishor Jha](https://www.drupal.org/u/kamlesh-kishor-jha)
